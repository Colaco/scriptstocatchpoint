const unirest = require('unirest')

const CATCHPOINT_REST_API = require('unirest')

const clientId = '';
const clientSecret = '';
const instant_test_object = '{"http_method":{"name":"GET","id":0},"id":0,"test_type":{"name":"Web","id":0},"monitor":{"id":18,"name":"Chrome"},"test_url":"https:\/\/google.com","nodes":[{ "name": "New York - Cogent", "id": 275 }, { "name": "Chicago - NTT", "id": 248 }],"advanced_settings":{"additional_settings":{"capture_http_headers":true}}}';

CATCHPOINT_REST_API
	.post('https://io.catchpoint.com/ui/api/token')
	.headers({
		'Content-Type': 'application/x-www-form-urlencoded',
		Accept: 'application/json'
	})
	.send('grant_type=client_credentials')
	.send('client_id=' + clientId)
	.send('client_secret=' + clientSecret)
	.then((response) => {
		console.log(response.body)
		var catchpointTokenResponse = response.body;

		if (catchpointTokenResponse.Message == 'invalid_request') {
            console.log("Invalid request")
            
		} else {
			var catchpointToken = catchpointTokenResponse.access_token;


			var buff = new Buffer(catchpointToken)
			var base64data = buff.toString('base64')
			CATCHPOINT_REST_API
				.post('https://io.catchpoint.com/ui/api/v1/onDemandTest/0')
				.headers({
					authorization: 'Bearer ' + base64data,
					Accept: 'application/json',
					'Content-Type': 'application/json'
				})
				.send(instant_test_object)
				.then((response) => {
					console.log('Instant test Response' + JSON.stringify(response.body))
				})

		}

	})